FROM registry.gitlab.com/chiavegatto/question-core-test:dependencies

COPY . .

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput

CMD ["python","-u", "production-server.py"]
